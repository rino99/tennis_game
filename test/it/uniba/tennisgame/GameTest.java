package it.uniba.tennisgame;
import static org.junit.Assert.*;
import org.junit.Test;

public class GameTest {
    @Test
    public void testFifteenThirty() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        String status = game.getGameStatus();
        //Asset
        assertEquals("Federer fifteen - Nadal thirty", status);
    }
    @Test
    public void testFortyThirt() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        String status = game.getGameStatus();
        //Asset
        assertEquals("Federer forty - Nadal thirty", status);
    }
    @Test
    public void testPlayer1Wins() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName1);
        //l'ordinde degli incrementi � importante altrimenti si solleva l'eccezione
        String status = game.getGameStatus();
        //Asset
        assertEquals("Federer wins", status);
    }
    @Test
    public void testFifteenForty() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        String status = game.getGameStatus();
        //Asset
        assertEquals("Federer fifteen - Nadal forty", status);
    }
    @Test
    public void testDeuce() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        String status = game.getGameStatus();
        //Asset
        assertEquals("Deuce", status);
    }
    @Test
    public void testAdvantagePlayer1() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName1);
        String status = game.getGameStatus();
        //Asset
        assertEquals("Advantage Federer", status);
    }
    @Test
    public void testPlayer2Wins() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        String status = game.getGameStatus();
        //Asset
        assertEquals("Nadal wins", status);
    }
    @Test
    public void testAdvantagePlayer2() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        String status = game.getGameStatus();
        //Asset
        assertEquals("Advantage Nadal", status);
    }
    @Test
    public void testDeuceAfterAdvantage() throws Exception {
        //Arrange
        Game game = new Game("Federer", "Nadal");
        String playerName1 = game.getPlayerName1(); 
        String playerName2 = game.getPlayerName2();
        //Act
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName1);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        game.incrementPlayerScore(playerName2);
        //qua sono pari
        game.incrementPlayerScore(playerName1);
        //qua � avanti player1
        game.incrementPlayerScore(playerName2);
        //qua � di nuovo pari
        String status = game.getGameStatus();
        //Asset
        assertEquals("Deuce", status);
    }
}