package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreSholdBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);		
		//Act
		player.incrementScore();
		//Assert
		assertEquals(1, player.getScore());
	}

	@Test
	public void scoreSholdNotBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);		
		//Act
		//player.incrementScore();
		//Assert
		assertEquals(0, player.getScore());
	}
	
	@Test
	public void scoreSholdBeLove() {
		//Arrange
		Player player = new Player("Federer", 0);		
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("love", scoreAsString);
	}
	
	@Test
	public void scoreSholdBeFifteen() {
		//Arrange
		Player player = new Player("Federer", 1);		
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("fifteen", scoreAsString);
	}
	
	@Test
	public void scoreSholdBeThirty() {
		//Arrange
		Player player = new Player("Federer", 2);		
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("thirty", scoreAsString);
	}
	
	@Test
	public void scoreSholdBeForty() {
		//Arrange
		Player player = new Player("Federer", 3);		
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("forty", scoreAsString);
	}
	
	@Test
	public void scoreSholdBeNullIfNegative() {
		//Arrange
		Player player = new Player("Federer", -1);		
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals(null, scoreAsString);
	}
	
	@Test
	public void scoreSholdBeNullIfMajor() {
		//Arrange
		Player player = new Player("Federer", 4);		
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals(null, scoreAsString);
	}
	
	@Test
	public void shouldBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 2);	
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean result = player1.isTieWith(player2);
		//Assert
		assertTrue(result);
	}
	
	@Test
	public void shoulNotdBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 2);	
		Player player2 = new Player("Nadal", 1);
		//Act
		boolean result = player1.isTieWith(player2);
		//Assert
		assertFalse(result);
	}
	
	@Test
	public void shoulBeHasAtLeastFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);	
		//Act
		boolean result = player1.hasAtLeastFortyPoints();
		//Assert
		assertTrue(result);
	}
	
	@Test
	public void shoulNotHasAtLeastFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 1);	
		//Act
		boolean result = player1.hasAtLeastFortyPoints();
		//Assert
		assertFalse(result);
	}
	
	@Test
	public void shoulHasLessThanFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 2);	
		//Act
		boolean result = player1.hasLessThanFortyPoints();
		//Assert
		assertTrue(result);
	}
	
	@Test
	public void shoulNotHasLessThanFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);	
		//Act
		boolean result = player1.hasLessThanFortyPoints();
		//Assert
		assertFalse(result);
	}
}

